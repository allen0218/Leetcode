# three numbers sum 
# 暴力解法，会超时
class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        n = len(nums)
        res = []
        
        if n < 3:
            return []
        
        for i in range(0, len(nums)):
            for j in range(0, len(nums)):
                for x in range(0, len(nums)):
                    s = []
                    if nums[i] + nums[j] + nums[x] == 0 and i!=j and j!=x and i!=x:
                        s.append(nums[i])
                        s.append(nums[j])
                        s.append(nums[x])
                        s.sort()
                        if s not in res:
                            res.append(s)
        return res


# three numbers sum 
# 双指针解法
class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        n = len(nums)
        res = []
        
        if n < 3:
            return []
        
        nums.sort()
        
        for i in range(0, len(nums)):
            if nums[i] > 0:
                return res
            
            if i > 0 and nums[i] == nums[i-1]:
                continue
                
            L = i + 1
            R = n - 1
            
            while R > L:
                if nums[i] + nums[L] + nums[R] == 0:
                    res.append([nums[i], nums[L], nums[R]])
                    while R > L and nums[L] == nums[L+1]:
                        L = L + 1 
                    while R > L and nums[R] == nums[R-1]:
                        R = R - 1
                    L = L + 1
                    R = R - 1
                elif nums[i] + nums[L] + nums[R] > 0:
                    R = R - 1
                else:
                    L = L + 1 
                    
        return res